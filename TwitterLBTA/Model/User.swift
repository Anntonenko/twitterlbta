//
//  User.swift
//  TwitterLBTA
//
//  Created by getTrickS2 on 4/9/18.
//  Copyright © 2018 Nik's. All rights reserved.
//

import UIKit

struct User {
    let name: String
    let username: String
    let bioText: String
    let profileImage: UIImage
}
