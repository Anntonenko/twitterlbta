//
//  HomeDatasource.swift
//  TwitterLBTA
//
//  Created by getTrickS2 on 4/9/18.
//  Copyright © 2018 Nik's. All rights reserved.
//

import LBTAComponents

class HomeDatasource: Datasource {
    let users: [User] = {
        let nikitaUser = User(name: "Nikita Antonenko", username: "@nikitaanntonenko", bioText: "iPhone, iPad, Data Science, Programming Community. Join to us to learn Python, Swift, R and build any interesting apps!", profileImage: #imageLiteral(resourceName: "nikita_profile_image"))
        let brianUser = User(name: "Brian Voong", username: "@buildthatapp", bioText: "iPhone, iPad, iOS Programming Community. Join to us to learn Swift, Objective-C R and build iOS apps!", profileImage: #imageLiteral(resourceName: "brian_profile_image"))
        let rayUser = User(name: "Ray Wenderlich", username: "@rwenderlich", bioText: "Ray Wenderlich is an iPhone developer and tweets on topics related to iPhone, software, and gaming. Check out our conference", profileImage: #imageLiteral(resourceName: "ray_profile_image"))
        
        return [nikitaUser, brianUser, rayUser]
    }()
    
    let tweets: [Tweet] = {
        let nikitaUser = User(name: "Nikita Antonenko", username: "@nikitaanntonenko", bioText: "iPhone, iPad, Data Science, Programming Community. Join to us to learn Python, Swift, R and build any interesting apps!", profileImage: #imageLiteral(resourceName: "nikita_profile_image"))
        let rayUser = User(name: "Ray Wenderlich", username: "@rwenderlich", bioText: "Ray Wenderlich is an iPhone developer and tweets on topics related to iPhone, software, and gaming. Check out our conference", profileImage: #imageLiteral(resourceName: "ray_profile_image"))
        let firstTweet = Tweet(user: nikitaUser, message: "Let's start to write the text here, because I need a long text blok to render out to the simulator. And I think we going to stope in a little time. So let's implement this app and stopes right here")
        let secondTweet = Tweet(user: rayUser, message: "This is a second tweet message for my sample project by Ray Wenderlich. This is very very exiting message and so on and so forth.")
        
        return [firstTweet, secondTweet]
    }()
    
    override func item(_ indexPath: IndexPath) -> Any? {
        switch indexPath.section {
        case 0: return users[indexPath.item]
        case 1: return tweets[indexPath.item]
        default: return nil
        }
    }
    override func numberOfSections() -> Int {
        return 2
    }
    override func numberOfItems(_ section: Int) -> Int {
        switch section {
        case 0: return users.count
        case 1: return tweets.count
        default: return 0
        }
    }
    override func headerClasses() -> [DatasourceCell.Type]? {
        return [UserHeader.self]
    }
    override func cellClasses() -> [DatasourceCell.Type] {
        return [UserCell.self, TweetCell.self]
    }
    override func footerClasses() -> [DatasourceCell.Type]? {
        return [UserFooter.self]
    }
    
}
