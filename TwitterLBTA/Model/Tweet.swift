//
//  Tweet.swift
//  TwitterLBTA
//
//  Created by getTrickS2 on 4/12/18.
//  Copyright © 2018 Nik's. All rights reserved.
//

import UIKit

struct Tweet {
    let user: User
    let message: String
}
