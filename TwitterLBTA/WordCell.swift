//
//  WordCell.swift
//  TwitterLBTA
//
//  Created by getTrickS2 on 4/8/18.
//  Copyright © 2018 Nik's. All rights reserved.
//

import UIKit

class WordCell: UICollectionViewCell {
    
    // MARK: - Variables ====================================
    let textLabel: UILabel = {
        let label = UILabel()
        label.text = "TEST TEST TEST"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    // ======================================================
    
    // MARK: - Inits ========================================
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // ======================================================
    
    // MARK: - Functions ====================================
    func setupViews() {
        backgroundColor = .yellow
        
        addSubview(textLabel)
        textLabel.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        textLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        textLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
    }
    // ======================================================
    
}
