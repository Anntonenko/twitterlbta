//
//  UserDescriptionTextView.swift
//  TwitterLBTA
//
//  Created by getTrickS2 on 4/12/18.
//  Copyright © 2018 Nik's. All rights reserved.
//

import UIKit

extension UITextView {

    func setupTextFromTweet(tweet: Tweet) {
        let attributedText = NSMutableAttributedString(string: tweet.user.name + "  ",
                                                       attributes: [NSAttributedStringKey.font : UIFont.boldSystemFont(ofSize: 16)])
        attributedText.append(NSAttributedString(string: tweet.user.username + "\n",
            attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 14),
                         NSAttributedStringKey.foregroundColor : UIColor(r: 130, g: 130, b: 130)]))
        // Add paragraphStyle
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        let range = NSMakeRange(0, attributedText.string.count)
        attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range)
        
        attributedText.append(NSAttributedString(string: tweet.message, attributes: [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 15)]))
        
        self.attributedText = attributedText
    }

}

