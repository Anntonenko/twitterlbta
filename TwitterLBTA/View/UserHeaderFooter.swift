//
//  UserCell.swift
//  TwitterLBTA
//
//  Created by getTrickS2 on 4/9/18.
//  Copyright © 2018 Nik's. All rights reserved.
//

import LBTAComponents

let twitterBlue = UIColor(r: 61, g: 167, b: 244)

class UserHeader: DatasourceCell {
    
    // MARK: - Items in cell ===================
    let textLabel: UILabel = {
        let label = UILabel()
        label.text = "WHO TO FOLLOW"
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()
    // =========================================
    
    override func setupViews() {
        super.setupViews()
        
        self.backgroundColor = .white
        self.separatorLineView.isHidden = false
        self.separatorLineView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        
        addSubview(textLabel)
        textLabel.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, leftConstant: 12)
    }
}

class UserFooter: DatasourceCell {
    
    // MARK: - Items in cell ===================
    let textLabel: UILabel = {
        let label = UILabel()
        label.text = "   Show me more..."
        label.backgroundColor = .white
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = twitterBlue
        return label
    }()
    // =========================================
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(textLabel)
        textLabel.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, bottomConstant: 14)
    }
}


































