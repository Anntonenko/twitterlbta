//
//  TweetCell.swift
//  TwitterLBTA
//
//  Created by getTrickS2 on 4/12/18.
//  Copyright © 2018 Nik's. All rights reserved.
//

import LBTAComponents

class TweetCell: DatasourceCell {
    
    override var datasourceItem: Any? {
        didSet {
            guard let tweet = datasourceItem as? Tweet else { return }
            profileImageView.image = tweet.user.profileImage
            messageTextView.setupTextFromTweet(tweet: tweet)
        }
    }

    // MARK: - Items in cell ===================
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 5
        imageView.clipsToBounds = true
        return imageView
    }()
    let messageTextView = UITextView()
    // =========================================
    
    
    override func setupViews() {
        super.setupViews()
        
        self.backgroundColor = .white
        self.separatorLineView.isHidden = false
        self.separatorLineView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        
        addSubview(profileImageView)
        addSubview(messageTextView)
        
        profileImageView.anchor(self.topAnchor, left: self.leftAnchor,
                                topConstant: 12, leftConstant: 12, widthConstant: 50, heightConstant: 50)
        messageTextView.anchor(self.topAnchor, left: profileImageView.rightAnchor, bottom: self.bottomAnchor, right: self.rightAnchor,
                               topConstant: 4, leftConstant: 4)
        
    }
    
}
